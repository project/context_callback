<?php

/**
 * Expose callback as a context condition.
 */
class context_condition_callback extends context_condition {

  protected $callback_info;

  public function __construct($plugin, $info) {
    parent::__construct($plugin, $info);

    $this->callbacks_info = module_invoke_all('context_callback_info');
  }

  function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');
    return array(
      'condition_mode' => array(
        '#title' => t('Require which conditions?'),
        '#type' => 'select',
        '#options' => array(
           CONTEXT_CONDITION_MODE_AND => t('All selected'),
           CONTEXT_CONDITION_MODE_OR => t('Any selected'),
           '' => t("Use the context's condition mode"),
        ),
        '#default_value' => isset($defaults['condition_mode']) ? $defaults['condition_mode'] : '',
      ),
    );
  }

  function condition_values() {
    $values = array();
    if (empty($this->callbacks_info['conditions'])) {
      return $values;
    }
    foreach ($this->callbacks_info['conditions'] as $callback_id => $callback_info) {
      $values[$callback_id] = check_plain($callback_info['label']);
    }
    return $values;
  }

  /**
   * Execute the condition. This triggers all callbacks.
   */
  public function execute() {
    foreach ($this->get_contexts() as $context) {
      $options = $this->fetch_from_context($context, 'options');
      $condition_mode = isset($options['condition_mode']) ? $options['condition_mode'] : '';
      if ($condition_mode === '') {
        $condition_mode = $context->condition_mode;
      }

      $condition_mode = $condition_mode == CONTEXT_CONDITION_MODE_AND;
      $condition_met = (bool) $condition_mode;

      $callbacks = $this->fetch_from_context($context, 'values');
      foreach ($callbacks as $callback_id) {
        if (!isset($this->callbacks_info['conditions'][$callback_id])) {
          continue;
        }
        $callback = $this->callbacks_info['conditions'][$callback_id]['callback'];
        if (function_exists($callback)) {
          if (empty($this->callbacks_info['conditions'][$callback_id]['callback arguments'])) {
            $callback_met = $callback();
          }
          else {
            $callback_arguments = $this->callbacks_info['conditions'][$callback_id]['callback arguments'];
            $callback_met = call_user_func_array($callback, $callback_arguments);
          }
          if ($condition_mode) {
            $condition_met = $condition_met && $callback_met;
          }
          else {
            $condition_met = $condition_met || $callback_met;
          }
        }
      }

      if ($condition_met) {
        $this->condition_met($context);
      }
    }
  }
}
